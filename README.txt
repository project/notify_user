Notify User

This is a simple light weight module that allows users to send notifications
to other site users from a node edit page. The message will include a link back
to the node in question and you can append additional information as well.
There is no additional configuration necessary after enabling the module.

After enabling, a new collapsible side bar item will appear on the node edit
page with fields to enter the user or users you wish to message as well as an
additional field to construct your own message.
